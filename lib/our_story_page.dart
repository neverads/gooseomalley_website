import 'package:flutter/material.dart';
import 'package:goose_omalley_website/main.dart';

class MyStoryPage extends StatelessWidget {
  const MyStoryPage({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("My story"),
        ),
        body: Center(
          child: SizedBox(
            width: globalWidth,
            child: ListView.builder(
              itemCount: myStory.length,
              padding: const EdgeInsets.only(bottom: 64),
              itemBuilder: (BuildContext context, int index) {
                  final StorySection storySection = myStory[index];
                  final bool isImageLeading = index.isEven;
                  return Padding(
                    padding: const EdgeInsets.all(16),
                    child: ListTile(
                      leading: isImageLeading ? storySection.icon : null,
                      title: Text(storySection.sentences.join('  ')),
                      trailing: isImageLeading ? null : storySection.icon,
                    ),
                  );
              },
            ),
          ),
        ),
      );
}

final List<StorySection> myStory = [
  const StorySection(
    icon: null,
    sentences: [
      "Hi, I'm Katie.",
    ],
  ),
  const StorySection(
    icon: Icon(Icons.person),
    sentences: [
      "I'm a ceramic screen printer (scroll to the bottom for a brief explanation of screen printing).",
      "I combine handmade ceramic jewelry and functional items with hand pulled screen prints using designs that I made or custom designs from you. ",
    ],
  ),
  const StorySection(
    icon: Icon(Icons.palette),
    sentences: [
      "I've always dabbled in art.",
      "I went to art school in Springfield, Missouri, USA, then life took me in many different directions.",
      "I began collecting more college degrees, in Baking & Pastry and Travel & Tourism.",
      "I became a pastry chef and a travel agent.",
      "Then my partner and I moved to Berlin, Germany, the fabulous city we now call home.",
      "I worked as an alternative walking tour guide, became a mom... "
          "throughout all of this, I kept creating art on the side.",
      "I could never quite figure out what I wanted to do with it.",
      "Then I found screen printing.",
      "It's such a versatile medium! I began printing on everything I could think of.",
      "The thing that I most wanted to print on, though, was pottery.",
      "I had loved my ceramics classes in school, but couldn't figure out exactly how to combine these two mediums.",
      "I started to talk to other artists in different countries who were doing things similar to what I wanted.",
      "The main problem was that the materials they were using weren't as accessible or practical here.",
      "So, I experimented with different papers for the transfers and different ingredients to make my own ink.",
      "I got into a lovely pottery studio and began making what you can see now on my Instagram, "
          "and someday soon, also on the gallery of this website.",
    ],
  ),
  const StorySection(
    icon: null,
    sentences: [
      "What I offer:",
    ],
  ),
  const StorySection(
    icon: Icon(Icons.bubble_chart),
    sentences: [
      "My porcelain jewelry ranges from small glazed rings to pendants and earrings that are screen printed with "
          "an abstract Matisse style tree design or decorated with pops of color using a bubbling technique.",
    ],
  ),
  const StorySection(
    icon: Icon(Icons.travel_explore),
    sentences: [
      "Many of my functional products combine these Matisse style trees and bubbling effect with screen printed "
          "Berlin monuments to create a unique piece for those who live in Berlin or want to take home a one of a "
          "kind souvenir to remember their trip to this wonderful city.",
    ],
  ),
  const StorySection(
    icon: Icon(Icons.dashboard_customize),
    sentences: [
      "All of these can be custom made with a variety of monuments and colors or with your own design or logo.",
      "I also create custom screen printed transfers for ceramic artists who want to put their own designs "
        "on their pottery.",
    ],
  ),
  const StorySection(
    icon: Icon(Icons.print_disabled),
    sentences: [
      "Screen Printing:  a process used to transfer designs onto a surface like fabric or paper.",
      "The design is put on the screen in a way that blocks everything on the screen except for the design.",
      "Ink is then pulled through the open parts of the screen.",
      "Each color in the design is a separate layer and requires a separate screen to be made.",
    ],
  ),
];

class StorySection {
  final Icon? icon;
  final List<String> sentences;

  const StorySection({
    required this.icon,
    required this.sentences,
  });
}
