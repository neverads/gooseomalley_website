import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:goose_omalley_website/our_story_page.dart';
import 'package:url_launcher/url_launcher.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: SizedBox(
            width: 240,
            child: ListView(
              padding: const EdgeInsets.only(bottom: 64),
              children: <Widget>[
                logo,
                buttonBox(
                  child: ElevatedButton.icon(
                    onPressed: () async => await Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const MyStoryPage(),
                      ),
                    ),
                    icon: const Icon(Icons.people_alt_outlined),
                    label: const Text("My story"),
                  ),
                ),
                spacer,
                buttonBox(
                  child: ElevatedButton.icon(
                    onPressed: () async => await launch('https://www.instagram.com/_gooslings_/'),
                    icon: const Icon(Icons.camera_alt_outlined),
                    label: const Text("Instagram"),
                  ),
                ),
                spacer,
                buttonBox(
                  child: const Center(
                    child: Text(
                      'Gallery & shop coming soon!',
                    ),
                  )
                ),
              ],
            ),
          ),
        ),
      );

  Padding get logo => Padding(
        padding: const EdgeInsets.symmetric(vertical: 32),
        child: SvgPicture.asset(
          'assets/logo.svg',
          width: 200,
          height: 200,
        ),
      );

  SizedBox buttonBox({required Widget child}) => SizedBox(
        height: 36,
        child: child,
      );

  SizedBox get spacer => const SizedBox(height: 16);

  Future<void> launch(String url) async {
    if (!await launchUrl(Uri.parse(url))) {
      // todo -- display a snack-bar on error
      // throw Exception('Could not launch $url');
    }
  }
}
